#include <TelegramKeyboard.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  TelegramKey::Inline key, keyStatus = {"Обновить","/status"};
  TelegramKey::Keyboard keys(2);
  key = { "Потушить","/off"};

  keys.addKey( key );
  keys.addKey( keyStatus );

  Serial.println( keys.toJson() );
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
