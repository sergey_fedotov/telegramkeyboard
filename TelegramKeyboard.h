#pragma once

namespace TelegramKey {

  struct Inline {
    const char * text;
    const char * data;

    // Inline(const char * text, const char * data, bool contact= false, bool location = false):
    //   text(text),data(data),request_contact(contact),request_location(location)
    // {};
    bool isEmpty() const {
      return nullptr == data || 0 == data[0];
    };
    String toJson() const {
      String s;
      const char * type = strncasecmp(data, (const char*)F("http"), 4) &&
                          strncasecmp(data, (const char*)F("tg:/"), 4)  ? 
        (const char*)F("callback_data") : 
        (const char*)F("url");
      s += F( "{\"text\":\"" ); 
      s += text; s+= "\",\"";
      s += type; s+= F("\":\""); s+= data;
      s += '"';

      s += '}';
      return s;
    };
  };
  struct Replay {
    const char * data;
    bool request_contact = false;
    bool request_location = false;

    bool isEmpty() const {
      return nullptr == data || 0 == data[0];
    };
    String toJson() const {
      String s;
      bool extendMode = request_contact || request_location;
      if ( extendMode )
        s += F( "{\"text\":");
      s += '"'; s += data; s += '"';
      if ( extendMode ){
        if ( request_contact )
          s += F( ",\"request_contact\":true" );
        else if ( request_location )
          s += F( ",\"request_location\":true" );      
        s += '}';
      }

      return s;
    }
  };
/*  inline String makeKeyboard(Inline k);
  inline String makeKeyboard(Replay k);

  template <typename T>  // first arg must be present, other args must have the same type
  String makeKeyboard(const uint8_t X, const uint8_t Y, T* keys ){
    String out;
    assert ( X > 0 && Y > 0 );
    uint index = 0;
    out += '[';
    for( uint8_t y = 0; y < Y; y++){
      if ( y != 0 ) {
        out += ',';
      }
      out += '[';
      for( uint8_t x = 0; x < X; x++){       
        if ( !keys[index].isEmpty() ){
          if ( x != 0 ){
            out += ',';
          }
          out += keys[index].toJson();      
        } 
       index++;         
       }
       out += ']';   
    }
    out += ']';
    #ifdef TELEGRAM_DEBUG
    Serial.println(out);
    #endif
    return out;
  };

  inline String makeKeyboard(Inline k){
    return TelegramKey::makeKeyboard(1,1, (Inline*)&k );
  };
  inline String makeKeyboard(Replay k){
    return TelegramKey::makeKeyboard(1,1, (Replay*)&k );
  };*/

  class Keyboard {
  private:
    const uint8_t Rows;
    String json ;
    uint8_t row ;
    uint8_t line ;
    bool finished = false;
    bool nextRow(){
      bool res;
      row += 1;
      res = row < Rows;
      if ( !res ) {
        line += 1;
        row = 0;
      }
      return res; 
    };

  public:
    Keyboard(uint8_t rows=1) :
      Rows(rows),json('['), row(0), line(0), finished(false)
    { };
    ~Keyboard(){};

    Keyboard* addKey(const char * keyJson = nullptr){
      assert ( !finished && F("You can't to add key to used keyboard"));
      if ( row == 0 ) {
        if ( line !=0 ) json += ',';
        json += '[';
      }
      else if ( keyJson != nullptr && keyJson[0] != 0 ) {//если нет данных или пустые, то пропускаем 
        json += ',';
      }
      json += keyJson;
      if ( !nextRow() ) json += ']';  
      return this;    
    };

    Keyboard* addKey(Inline key){
      return addKey( key.toJson().c_str() );     
    };

    Keyboard* addKey(Replay key){
      return addKey( key.toJson().c_str() );     
    };

    String toJson(){
      if ( !finished ){
        if ( row != 0 ) json += ']'; 
        json += ']';
        finished = !finished;
      }
      //Serial.println(json);
      return json;
    };
  }; // class Keyboard
template <typename T> inline Keyboard& operator << (Keyboard &k, T key){
      return *k.addKey(key); 
    };

}; // TelegramKey